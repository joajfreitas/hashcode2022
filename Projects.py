class Project:
    def __init__(self, name, duration, score, best_before, roles):
        self.name = name
        self.duration = duration
        self.score = score
        self.best_before = best_before
        self.roles = roles
        self.complete = 0

    #def __repr__(self):
    #    return (
    #        f"{self.name} {self.duration} {self.score} {self.best_before} {self.roles}"
    #    )

    def duration_update(self):
        self.duration = self.duration - 1

    def is_complete(self):
        return self.complete

    def start(self):
        self.complete = 1
