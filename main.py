import sys
from Contributors import Contributor
from Projects import Project


def pairwise(iterable):
    "s -> (s0, s1), (s2, s3), (s4, s5), ..."
    a = iter(iterable)
    return zip(a, a)


def read_contributor(lines):
    name, n_skills = lines.pop(0).split(" ")
    skills = []
    for i in range(int(n_skills)):
        skill, level = lines.pop(0).split(" ")
        skills.append((skill, int(level)))

    contrib = Contributor(name, skills)

    return contrib


def read_project(lines):
    name, duration, score, best_before, n_roles = lines.pop(0).split(" ")

    skills = []
    for i in range(int(n_roles)):
        n, level = lines.pop(0).split(" ")
        skills.append((n, int(level)))

    project = Project(name, int(duration), int(score), int(best_before), skills)

    return project


def read_input():
    with open(sys.argv[1]) as f:
        lines = f.readlines()

    n_contrib, n_projects = [int(x) for x in lines.pop(0).split()]

    contributors = []
    for i in range(n_contrib):
        contributors.append(read_contributor(lines))

    projects = []
    for i in range(n_projects):
        projects.append(read_project(lines))

    return contributors, projects


def max_simulation_time(projects):
    return max([project.best_before + project.score for project in projects])


def fom(t, project):
    actual_score = max(t + project.duration - project.best_before, 0) + project.score
    return actual_score / project.duration


def foms(t, projects):
    for project in projects:
        yield fom(t, project)


def write_output(proj, contribs):
    names = ""
    
    skill_order = [role[0] for role in proj.roles]

    contribs = [(contrib, skill) for contrib, skill in contribs.items()]
    for contrib in sorted(contribs, key=lambda x: skill_order.index(x[1])):
        names += contrib[0].name + " "

    return proj.name, names


# sort list of projects
def funcDaniels(fom):
    li = []
    for i in range(len(fom)):
        li.append([fom[i], i])
    li.sort()
    sort_index = []

    for x in li:
        sort_index.append(x[1])

    return sort_index


def normals(project, contributors):
    xs = {role[0]: [] for role in project.roles}

    for contributor in contributors:
        for skill, level in contributor.skills:
            if (skill, level) in project.roles:
                xs[skill].append(contributor)

    return xs


def juniors(project, contributors):
    xs = {role[0]: [] for role in project.roles}

    for contributor in contributors:
        for skill, level in contributor.skills:
            if (skill, level - 1) in project.roles:
                xs[skill].append(contributor)

    return xs


def seniors(project, contributors):
    xs = {role[0]: [] for role in project.roles}

    for contributor in contributors:
        for skill, level in contributor.skills:
            for role in project.roles:
                if skill == role[0] and level > role[1]:
                    xs[skill].append(contributor)

    return xs


def reduced_list_of_contrib(project, contributors):
    free_contributors = [contributor for contributor in contributors if not contributor.is_allocated()]

    ns = normals(project, free_contributors)
    js = juniors(project, free_contributors)
    ss = seniors(project, free_contributors)

    return ns, js, ss


def checkPossible(freitas_normais, freitas_jr, freitas_sr):
    skills_done = {}
    
    
    
    for skill in freitas_normais:
        for gajo in freitas_normais[skill]:
            if gajo not in skills_done:
                skills_done[gajo]=skill
                
    for skill in freitas_sr:
        for gajo in freitas_sr[skill]:
            if gajo not in skills_done:
                skills_done[gajo]=skill
                
    for skill in freitas_jr:
        if skill not in skills_done:
            for gajo in freitas_jr[skill]:
                for gajo_sr in skills_done:
                    if gajo_sr in freitas_sr[skill] or gajo_sr in freitas_normais[skill]:
                        skills_done[gajo]=skill
                
    if len(skills_done) == len(freitas_normais):
        return skills_done
    else:
        return {}

def checkPossible2(freitas_normais, freitas_jr, freitas_sr):
    skills_done = {}
    
    for skill in freitas_jr:
        if skill not in skills_done:
            for gajo in freitas_jr[skill]:
                for gajo_sr in freitas_normais[skill]:
                    skills_done[gajo]=skill
                if skill not in skills_done:
                    for gajo_sr in freitas_sr[skill]:
                        skills_done[gajo]=skill
    
    for skill in freitas_normais:
        for gajo in freitas_normais[skill]:
            if gajo not in skills_done:
                skills_done[gajo]=skill
                
    for skill in freitas_sr:
        for gajo in freitas_sr[skill]:
            if gajo not in skills_done:
                skills_done[gajo]=skill
                
    
                
    if len(skills_done) == len(freitas_normais):
        return skills_done
    else:
        return []
    
def main():
    contributors, projects = read_input()

    solution = []
    
    for t in range(max_simulation_time(projects)):
        ## Compute FOM for each project that is still open
        fs = list(foms(t, projects))

        sortedProjects = funcDaniels(fs)
        ## For all the open projects ordered by FOM
        ## Compute if it is possible

        dropped_projects = []
        for index in sortedProjects:
            normals, seniors, juniors = reduced_list_of_contrib(
                projects[index], contributors
            )

            # laura:
            selected_contributors = checkPossible(normals, seniors, juniors)

            ## If possible
            if len(selected_contributors) != 0:
                ## Assign contributors to project!
                for contributor in selected_contributors.keys():
                    contributor.allocate(projects[index].duration)

                solution.append(write_output(projects[index], selected_contributors))
    
                dropped_projects.append(index)
            ## Increment skils
            for contributor, skill in selected_contributors.items():
                i = [skills[0] for skills in contributor.skills].index(skill)
                roles = {role[0]: role[1] for role in projects[index].roles}
                if contributor.skills[i][1] <= roles[skill]:
                    contributor.skills[i] = (contributor.skills[i][0], contributor.skills[i][1] + 1)


        for index in reversed(sorted(dropped_projects)):
            projects.pop(index)
            fs.pop(index)

        for contributor in contributors:
            contributor.dec_allocted()
    
    with open(sys.argv[2], "w") as f:
        f.write(str(len(solution)) + "\n")
        for sol in solution:
            f.write("\n".join(sol) + "\n")

if __name__ == "__main__":
    main()
