class Contributor:
    def __init__(self, name, skills):
        self.name = name
        self.skills = skills
        self.allocated = 0

    def __repr__(self):
        rep = self.name
        for skill, level in self.skills:
            rep = rep + " " + skill + " " + str(level)
        # for task in self.task:

        return rep

    def dec_allocted(self):
        self.allocated = self.allocated - 1

    def is_allocated(self):
        return self.allocated > 0

    def allocate(self, time):
        self.allocated = time
